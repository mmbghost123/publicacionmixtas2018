import { MercadosPage } from './app.po';

describe('mercados App', () => {
  let page: MercadosPage;

  beforeEach(() => {
    page = new MercadosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
