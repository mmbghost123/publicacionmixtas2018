
--CREA TABLA PUBLICACION MIXTA

CREATE TABLE [dbo].[PublicacionMixta](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[numeroCaso] [bigint] NULL,
	[gestion] [numeric](18, 0) NULL,
	[registroTributario] [varchar](50) NULL,
	[pmc] [varchar](100) NULL,
	[razonSocialContribuyente] [varchar](200) NULL,
	[primerNombre] [varchar](200) NULL,
	[segundoNombre] [varchar](200) NULL,
	[tercerNombre] [varchar](200) NULL,
	[primerApellido] [varchar](200) NULL,
	[segundoApellido] [varchar](200) NULL,
	[tercerApellido] [nchar](10) NULL,
	[documentoIdentidad] [varchar](200) NULL,
	[expedido] [varchar](10) NULL,
	[tipoDocumento] [varchar](50) NULL,
	[tipoContribuyente] [varchar](10) NULL,
	[Monto2008] [bigint] NULL,
	[Monto2009] [bigint] NULL,
	[Monto2010] [bigint] NULL,
	[Monto2011] [bigint] NULL,
	[Monto2012] [bigint] NULL,
	[Monto2013] [bigint] NULL,
	[Monto2014] [bigint] NULL,
	[Monto2015] [bigint] NULL,
	[MontoTotal] [bigint] NULL,
	[tipo] [varchar](100) NULL,
	[gestiones] [varchar](200) NULL,
	[observacion] [varchar](150) NULL,
	[estado] [varchar](10) NULL
) ON [PRIMARY]

--///////////////////////////////////////////////////////////

--//CREA TABLA BITACORA DE CONSULTAS DE LA PUBLICACION MIXTA
CREATE TABLE [dbo].[BitacoraPublicacionMixta](
	[documentoIdentidad] [varchar](50) NULL,
	[registroTributario] [varchar](50) NULL,
	[tipoContribuyente] [varchar](10) NULL,
	[tipo] [varchar](50) NULL,
	[fecha_consulta] [datetime] NULL CONSTRAINT [DF_FTM_VISITAS_FECHA_INSERCION]  DEFAULT (getdate())
) ON [PRIMARY]
--////////////////////////////////////////////////////////

--//PRCEDIMIENTO ALMACENADO PARA CONSULTAR LAS PUBLICACIONES MIXTA Y REGISTRO DE LA BITACORA DE CONSULTAS MIXTA
CREATE PROCEDURE [AE].[spConsultaMixta]
--CREATE PROCEDURE [GENERAL].[spContribuyente]
(
	@documentoIdentidad VARCHAR(200) = NULL,
	@registroTributario VARCHAR(50) = NULL,
	@razonSocialContribuyente VARCHAR(200) = NULL,
	@primerNombre VARCHAR(200) = NULL,
	@primerApellido vARCHAR(200) = NULL,
	@segundoApellido VARCHAR(200) = NULL,
	@tipoContribuyente VARCHAR(10) = NULL,
	@tipo VARCHAR(50) = NULL
)
AS

	SET NOCOUNT ON

-- =============================================
--  CONSULTAS
-- =============================================
-- ----------------------------------------------------------------
--  OBTENER LOS REGISTROS DE CONTRIBUYENTES NATURALES
-- ----------------------------------------------------------------
DECLARE @sql2 VARCHAR(200)	
IF (@tipo='Todos')
BEGIN
	SET @sql2=''
END
ELSE
BEGIN
	SET @sql2=' AND tipo = ''' + LTRIM(RTRIM(@tipo)+ '''')
END
DECLARE @sql1 NVARCHAR(MAX)	
IF (@tipoContribuyente = 'N')
BEGIN

	

	SET @sql1 = '
	   SELECT    tipo, numeroCaso, gestion, registroTributario, razonSocialContribuyente, gestiones, MontoTotal
       FROM         PublicacionMixta
	   WHERE tipoContribuyente = ''N'''

	IF (@documentoIdentidad IS NOT NULL AND @documentoIdentidad <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND documentoIdentidad = ''' + LTRIM(RTRIM(@documentoIdentidad)+ '''') 
	END
	
	IF (@registroTributario IS NOT NULL AND @registroTributario <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND registroTributario = ''' + LTRIM(RTRIM(@registroTributario)+ '''') 
	END

	IF (@primerNombre IS NOT NULL AND @primerNombre <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND primerNombre LIKE ''%' + LTRIM(RTRIM(@primerNombre)) + '%'''
	END

	IF (@primerApellido IS NOT NULL AND @primerApellido <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND primerApellido LIKE ''%' + LTRIM(RTRIM(@primerApellido)) + '%'''
	END

	IF (@segundoApellido IS NOT NULL AND @segundoApellido <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND segundoApellido LIKE ''%' + LTRIM(RTRIM(@segundoApellido)) + '%'''
	END

	SET @sql1 = LTRIM(RTRIM(@sql1)) + @sql2 +' ORDER BY primerApellido, segundoApellido, primerNombre'

END
-- ----------------------------------------------------------------------------
--  OBTENER LOS REGISTROS DE CONTRIBUYENTES JURIDICOS
-- ----------------------------------------------------------------------------
ELSE
BEGIN


	SET @sql1 = '
	SELECT    tipo, numeroCaso, gestion, registroTributario, razonSocialContribuyente, gestiones, MontoTotal
       FROM         PublicacionMixta
	   WHERE tipoContribuyente = ''J'''
   IF (@documentoIdentidad IS NOT NULL AND @documentoIdentidad <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND documentoIdentidad = ''' + LTRIM(RTRIM(@documentoIdentidad)+ '''')
	END
	IF (@registroTributario IS NOT NULL AND @registroTributario <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND registroTributario = ''' + LTRIM(RTRIM(@registroTributario)+ '''') 
	END
	IF (@razonSocialContribuyente IS NOT NULL AND @razonSocialContribuyente <> '')
	BEGIN
		SET @sql1 = @sql1 + ' AND razonSocialContribuyente LIKE ''%' + LTRIM(RTRIM(@razonSocialContribuyente)) + '%'''
	END
	SET @sql1 = LTRIM(RTRIM(@sql1)) + @sql2 +' ORDER BY razonSocialContribuyente'
		
END
-- ----------------------------------------------------------------------------
--  ALMACENA EN LA BITACORA DE CONSULTAS MIXTAS
-- ----------------------------------------------------------------------------
INSERT INTO BitacoraPublicacionMixta
           (documentoIdentidad
           ,registroTributario
			,tipoContribuyente
           ,tipo
           )
     VALUES
           (@documentoIdentidad
           ,@registroTributario
			,@tipoContribuyente
           ,@tipo)
-- ----------------------------------------------------------------------------
--  EJECUTA CONSULTA DE CONSULTA
-- ----------------------------------------------------------------------------
	EXEC(@sql1)
	
--//////////////////////////////////////////////////////////////////////////////////////////////////////

