-- drop table grm_titulares
CREATE TABLE grm_titulares
(
  titular_id serial PRIMARY KEY,
  titular_tipo_documento character varying(15) NOT NULL,
  titular_ci character varying(10)  NOT NULL,
  titular_complemento character varying(5) ,
  titular_expedido character varying(10) ,
  titular_datos jsonb NULL,
  titular_estado character varying(5) DEFAULT 'A'::character varying,
  titular_registrado timestamp without time zone NOT NULL DEFAULT now(),
  titular_modificado timestamp without time zone NOT NULL DEFAULT now(),
  titular_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying
);

-- drop table grm_puestos

CREATE TABLE grm_puestos
(
  puesto_id serial NOT NULL ,
  puesto_titular_id integer NOT NULL,
  puesto_datos_tecnicos jsonb  NOT NULL,
  puesto_datos_ubicacion jsonb NULL,
  puesto_datos_adjuntos jsonb NULL,
  puesto_estado character varying(2) DEFAULT 'A'::character varying,
  puesto_registrado timestamp without time zone NOT NULL DEFAULT now(),
  puesto_modificado timestamp without time zone NOT NULL DEFAULT now(),
  puesto_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
  CONSTRAINT grm_puestos_pkey PRIMARY KEY (puesto_id),
  CONSTRAINT grm_titulares_titular_idfk FOREIGN KEY (puesto_titular_id)
      REFERENCES public.grm_titulares (titular_id)
);

-- drop table grm_deudas
CREATE TABLE grm_deudas
(
  deuda_id serial NOT NULL  ,
  deuda_puesto_id integer NOT NULL,
  deuda_datos_tecnicos jsonb  NOT NULL,
  deuda_estado character varying(2) DEFAULT 'A'::character varying,
  deuda_registrado timestamp without time zone NOT NULL DEFAULT now(),
  deuda_modificado timestamp without time zone NOT NULL DEFAULT now(),
  deuda_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
    CONSTRAINT grm_deudas_pkey PRIMARY KEY (deuda_id),
    CONSTRAINT grm_puestos_deuda_puesto_idfk FOREIGN KEY (deuda_puesto_id)
    REFERENCES public.grm_puestos (puesto_id)
);
-- drop table grm_solicitudes
CREATE TABLE grm_solicitudes
(
  solicitud_id serial NOT NULL  ,
  solicitud_datos jsonb  NOT NULL,
  solicitud_estado character varying(2) DEFAULT 'A'::character varying,
  solicitud_registrado timestamp without time zone NOT NULL DEFAULT now(),
  solicitud_modificado timestamp without time zone NOT NULL DEFAULT now(),
  solicitud_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
    CONSTRAINT grm_solicitudes_pkey PRIMARY KEY (solicitud_id)
);

CREATE TABLE grm_tipo_actividad
(
  tipo_actividad_id serial NOT NULL primary Key,
  tipo_actividad_datos jsonb null,
  tipo_actividad_estado character varying(2) DEFAULT 'A'::character varying,
  tipo_actividad_modificado timestamp without time zone NOT NULL DEFAULT now(),
  tipo_actividad_registrado timestamp without time zone NOT NULL DEFAULT now(),
  tipo_actividad_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying
);

CREATE TABLE grm_gestiones
(
  gestion_id serial NOT NULL primary Key,
  gestion_valor integer not null,
  gestion_vencimiento timestamp without time zone NOT NULL,
  gestion_estado character varying(2) DEFAULT 'A'::character varying,
  gestion_modificado timestamp without time zone NOT NULL DEFAULT now(),
  gestion_registrado timestamp without time zone NOT NULL DEFAULT now(),
  gestion_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying
);

CREATE TABLE grm_tipo_puesto
(
  tipo_puesto_id serial not null,
  tipo_puesto_cod character varying(11) not null,
  tipo_puesto_descripcion text,
  tipo_puesto_estado character varying(1) DEFAULT 'A'::character varying,
  tipo_puesto_registrado timestamp without time zone NOT NULL DEFAULT now(),
  tipo_puesto_modificado timestamp without time zone NOT NULL DEFAULT now(),
  tipo_puesto_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
  primary key(tipo_puesto_id)
);

CREATE TABLE grm_tipo_mueble(
  tipo_mueble_id serial not null,
  tipo_mueble_cod character  varying(15) not null,
  tipo_mueble_descripcion text,
  tipo_mueble_estado character varying(1) DEFAULT 'A'::character varying,
  tipo_mueble_registrado timestamp without time zone NOT NULL DEFAULT now(),
  tipo_mueble_modificado timestamp without time zone NOT NULL DEFAULT now(),
  tipo_mueble_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
  primary key(tipo_mueble_id)
);

  create table grm_catalogo(
   catalogo_id serial not null,
   catalogo_codigo character  varying(20) not null,
   catalogo_descripcion character  varying(40) not null,
   catalogo_tipo character  varying(20) not null,
   catalogo_observaciones character  varying(40) not null,
   catalogo_estado character varying(1) DEFAULT 'A'::character varying,
   catalogo_registrado timestamp without time zone NOT NULL DEFAULT now(),
   catalogo_modificado timestamp without time zone NOT NULL DEFAULT now(),
   catalogo_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
   primary key(catalogo_id )
  );

  CREATE TABLE grm_asociaciones
(
 scc_id serial NOT NULL,
 scc_codigo character varying(11) NOT NULL,
 scc_descripcion text NOT NULL,
 scc_datos_adjuntos jsonb,
 scc_estado character varying(11) DEFAULT 'A'::character varying,
 scc_registrado timestamp without time zone NOT NULL DEFAULT now(),
 scc_modificado timestamp without time zone NOT NULL DEFAULT now(),
 scc_usuario character varying(20) NOT NULL DEFAULT 'ADMIN'::character varying,
 CONSTRAINT grm_asociaciones_pkey PRIMARY KEY (scc_id)
);


-- DROP FUNCTION public.jsonb_merge(jsonb, jsonb);

CREATE OR REPLACE FUNCTION public.jsonb_merge(
    jsonb,
    jsonb)
  RETURNS jsonb AS
$BODY$
WITH json_union AS (
    SELECT * FROM JSONB_EACH($1)
    UNION ALL
    SELECT * FROM JSONB_EACH($2)
) SELECT JSON_OBJECT_AGG(key, value)::JSONB
     FROM json_union
     WHERE key NOT IN (SELECT key FROM json_union WHERE value ='null');
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION public.jsonb_merge(jsonb, jsonb)
  OWNER TO postgres;

------------------Puesto
-- DROP FUNCTION public.obtenerdatos(jsonb);

CREATE OR REPLACE FUNCTION public.obtenerdatos(datos jsonb)
  RETURNS text AS
$BODY$

BEGIN    
  RETURN datos->'titular_datos'::text;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.obtenerdatos(jsonb)
  OWNER TO postgres;
-- DROP FUNCTION public.obtenerdatosadjuntostecnicos(jsonb);

CREATE OR REPLACE FUNCTION public.obtenerdatosadjuntostecnicos(datos jsonb)
  RETURNS text AS
$BODY$

BEGIN    
  RETURN datos->'puesto_datos_tecnicos'::text;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.obtenerdatosadjuntostecnicos(jsonb)
  OWNER TO postgres;
-- DROP FUNCTION public.obtenerdatosadjuntosubicacion(jsonb);

CREATE OR REPLACE FUNCTION public.obtenerdatosadjuntosubicacion(datos jsonb)
  RETURNS text AS
$BODY$

BEGIN    
  RETURN datos->'puesto_datos_ubicacion'::text;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.obtenerdatosadjuntosubicacion(jsonb)
  OWNER TO postgres;
-- DROP FUNCTION public.sp_modifica_puestotecnico(jsonb);

CREATE OR REPLACE FUNCTION public.sp_modifica_puestotecnico(datos jsonb)
  RETURNS boolean AS
$BODY$
BEGIN
     
  update grm_puestos set puesto_datos_tecnicos= jsonb_merge(puesto_datos_tecnicos,obtenerdatosadjuntostecnicos(datos)::jsonb)
  where puesto_id= obtIdPuesto(datos);

    RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sp_modifica_puestotecnico(jsonb)
  OWNER TO postgres;
-- DROP FUNCTION public.sp_modifica_puestoubicacion(jsonb);

CREATE OR REPLACE FUNCTION public.sp_modifica_puestoubicacion(datos jsonb)
  RETURNS boolean AS
$BODY$
BEGIN
     
      update grm_puestos 
      set puesto_datos_ubicacion= jsonb_merge(puesto_datos_ubicacion,obtenerdatosadjuntosubicacion(datos)::jsonb)
      where puesto_id= obtIdPuesto(datos);

     RETURN true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sp_modifica_puestoubicacion(jsonb)
  OWNER TO postgres;

CREATE OR REPLACE FUNCTION sp_obtener(sql text)
  RETURNS SETOF json AS
$BODY$
begin
sql := 'SELECT array_to_json(array_agg(row_to_json(d)))
     FROM (
            '||sql||'
        ) d ' ;
return query execute sql;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE;

-- DROP FUNCTION public.sp_obtener_puesto(integer);

CREATE OR REPLACE FUNCTION public.sp_obtener_puesto(id integer)
  RETURNS jsonb AS
$BODY$
declare
datos jsonb;
begin
     datos:= sp_obtener('select * from grm_puestos where puesto_id ='||id);
    return datos::jsonb;
   
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.sp_obtener_puesto(integer)
  OWNER TO postgres;
  
