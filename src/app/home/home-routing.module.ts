import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DeudaMoraComponent } from './deudamora/deudamora.component';
import { InmueblesComponent } from './inmuebles/inmuebles.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { VehiculosComponent } from './vehiculos/vehiculos.component';



const homeRoutes: Routes = [
  //{ path: 'home',  component: HomeComponent },
  //{ path: 'home/app1',  component: App1Component },
  { path: 'home', component: HomeComponent, 
    children: [
        {path: 'deudamora', component: DeudaMoraComponent, outlet:'firstchild'},
       {path: 'inmuebles', component: InmueblesComponent, outlet:'firstchild'},
       {path: 'actividades', component: ActividadesComponent, outlet:'firstchild'},//,
       {path: 'vehiculos', component: VehiculosComponent, outlet:'firstchild'}


        //{path: ':id', component: SecondchildComponent, outlet:'secondchild'}
    ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
  //declarations: [DeudaMoraComponent, App1Component, App2Component, App3Component, App4Component, App5Component]
})
export class HomeRoutingModule { }
