export class ModeloContribuyente {
    idActividadEconomica: number;
    gestion: string;
    tipo: string;
    idContribuyente: string;
    clase: string;
    padron: string;
    sucursal: string;
    vencimiento: string;
    ufvVencimiento: string;
    patente: string;
    total: string;
    patenteUfv: string;
    interesUfv: string;
    incumplimientoUfv: string;
    totalUfv: string;
    tipoDeuda: string;
    perdonazo: string;
    registro: string;
}