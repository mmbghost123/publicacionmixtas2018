export class ModeloContribuyente {
    IdActividad: number;
    Nro: string;
    FechaInicio: string;
    Descripcion: string;
    Direccion: string;
    Estado: string;
    Publicidades: string;
    fechaBaja: string;
    tipoBloqueo: string;
    numeroLicencia: string;
    idAE: string;
}