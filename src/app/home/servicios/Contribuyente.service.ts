import { Injectable } from '@angular/core';
//import { Component, OnInit } from '@angular/core';

import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';  
//import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';

import { ModeloContribuyente } from '../modelos/modeloContribuyente';


@Injectable()
export class ContribuyenteService {
    ///////////CJ
   // private AEConsultaMixtaUrl  = 'http://localhost:9094/v.01/Contribuyente/getConsultaMixta';

//private AEConsultaMixtaUrl  = 'http://192.168.32.169:9094/v.01/Contribuyente/getConsultaMixta';
//private AEConsultaMixtaUrl  = 'http://fiscagis:9094/v.01/Contribuyente/getConsultaMixta';
private AEConsultaMixtaUrl  = 'http://52.226.130.135:90/v.01/Contribuyente/getConsultaMixta';

   // private AEConsultaMixtaUrl  = 'http://52.226.130.135:90/v.01/Contribuyente/getConsultaMixta';
    
    constructor(private http: Http) {

     }
    public getConsultaMixta(DocumentoIdentidad: string, RegistroTributario: string, RazonSocialContribuyente: string, PrimerNombre: string, PrimerApellido: string, SegundoApellido: string, TipoContribuyente: string, Tipo: string): any {
        
//console.log('id1:'+ DocumentoIdentidad+ 'vartipo:' + Tipo);
        return this.http.post(this.AEConsultaMixtaUrl, { documentoIdentidad:DocumentoIdentidad, registroTributario: RegistroTributario, razonSocialContribuyente: RazonSocialContribuyente, primerNombre:PrimerNombre, primerApellido:PrimerApellido, segundoApellido:SegundoApellido, tipoContribuyente:TipoContribuyente, tipo:Tipo})
            .map(this.extractData)
            .catch(this.handleError);

    }

    
    private extractData(res: Response) {  
        let body = res.json();
        //alert ("HoLa"+" "+body.success[0].numeroReg);
        //alert ("HoLa"+" "+body.success[0].IDPERSONA);
        return body.success.dataSql;
    }


    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
         console.log('errrrrror:'+ error);
        let errMsg: string;
        if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg =  body.error.message;//`${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.error.message;//error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
      }

  
}

