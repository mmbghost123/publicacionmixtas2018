7//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { ModeloContribuyente } from '../modelos/modeloContribuyente';

import { LocalDataSource } from 'ng2-smart-table';

import { Popup } from 'ng2-opd-popup';

import { ContribuyenteService } from '../servicios/Contribuyente.service';



@Component({
  selector: 'app-root',
  templateUrl: './deudamora.component.html',
  styleUrls: ['./deudamora.component.css'],
  providers: [ContribuyenteService]
})

export class DeudaMoraComponent implements OnInit{

  public source: LocalDataSource;
  public source1: LocalDataSource;
  public source2: LocalDataSource;
  public source3: LocalDataSource;
  public source4: LocalDataSource;
  public source5: LocalDataSource;
  public source6: LocalDataSource;
  public source7: LocalDataSource;

  @ViewChild('popup1') popup1: Popup;
  @ViewChild('popup2') popup2: Popup;
  //datos1: Modelo1 [] = [];

  datos1: any [] = [];
  datos2: any [] = [];
  datos3: any [] = [];
datos6: any [] = [];
datos7: any [] = [];
  CGestion = [
    {name: '2017', abbrev: '2017'},
    {name: '2016', abbrev: '2016'},
    {name: '2015', abbrev: '2015'}
  ];


  CRubro = [
    {name: 'Todos', abbrev: 'T'},
    {name: 'Inmueble', abbrev: 'I'},
    {name: 'Vehiculo', abbrev: 'V'},
    {name: 'Actividade Economica', abbrev: 'A'},
    {name: 'Publicidad', abbrev: 'P'}
    
    
  ];

  var_data: ModeloContribuyente;

  errorMessage: any;
  Message: string;

  settings = {
    delete: {
      confirmDelete: false,
    },
    add: {
      inputClass: '',
      addButtonContent: 'Nuevo',
      createButtonContent: 'Crear',
      cancelButtonContent: 'Cancelar',
      confirmCreate: false,
    },
    edit: {
      confirmSave: false,
      inputClass: '',
      editButtonContent: 'Modificar',
      saveButtonContent: 'Actualizar',
      cancelButtonContent: 'Cancelar',
    },
    columns: {
      idPersona: { title: 'Código',},
      primerApellido: { title: 'Paternoooo',},
      segundoApellido: { title: 'Materno',},
      primerNombre: { title: 'Nombre',},
      padron: { title: 'padron',},
      identificacion: { title: 'Identificación',},
      estado: { title: 'Estado',}

    }
  };



   settings7 = {
    delete: {
      confirmDelete: false,
    },
    add: {
      labelClass: '',
      addButtonContent: 'Nuevo',
      createButtonContent: 'Crear',
      cancelButtonContent: 'Cancelar',
      confirmCreate: false,
      visible: false,
    },
    edit: {
      confirmSave: false,
      inputClass: '',
      editButtonContent: 'Modificar',
      saveButtonContent: 'Actualizar',
      cancelButtonContent: 'Cancelar',
    },
    columns: {
      tipo: { title: 'Rubro',},
      numerocaso: { title: 'Numero Caso',},
      gestion: { title: 'Gestion Publicación',},
      registrotributario: { title: 'Registro Tributario',},
      razonsocialcontribuyente: { title: 'Contribuyente',},
      gestiones: { title: 'Gestiones Liquidadas',},
      montototal: { title: 'Monto Total',},
      observacion: { title: 'Observacion',}
    }
  };
 


  personal: Personal = {
    ci: '',
    nombre: '',
    reg: '',
    paterno: '',
    materno: '',
    nit: '',
    empresa: '',
    padron: '',
    caso: ''
  };

  var_PersonaID = "";
  personaEstado='Natural';
  botonEstado='Aqui para Buscar Natural';

  subBusqueda = true;
  subPersona = true;
  subTabla = false;
  subTabla2 = false;
  subActividad = true;
  subCaso = false;
  subPopup= false;
  subPopup2= false;


  var_id="";
  var_paterno= "";
  var_materno= "";
  var_nombre= "";
  var_identificacion= "";
  var_reg= "";
  var_direccion= "";
  var_idA="";
  var_num="";
  var_descripcion="";

  constructor (public ContribuyenteService:ContribuyenteService){}

  public ngOnInit (){
    this.source = new LocalDataSource(this.datos1);
    this.source1 = new LocalDataSource(this.datos1);
    this.source2 = new LocalDataSource(this.datos2);
    this.source3 = new LocalDataSource(this.datos3);
    this.source4 = new LocalDataSource(this.datos2);
    this.source5 = new LocalDataSource(this.datos3);
    this.source6 = new LocalDataSource(this.datos6);
    this.source7 = new LocalDataSource(this.datos7);
    this.pintarPersona();
    this.getData();

  }

   public mensaje (mensaje: string):void {
       this.retornoError('AYUDA',mensaje)}

   public buscarContribuyenteMixta (resNI: string,resRT: string,resNIT: string,resRubro: any):void {
   
    let T: string = 'TODOS';
    
  if(resRubro.state=='T' || resRubro.state=='' ){T='TODOS';}
  else if (resRubro.state=='I') {T='INMUEBLE';}
  else if (resRubro.state=='V') {T='VEHICULO';}
  else if (resRubro.state=='A') {T='PATENTE ACTIVIDAD ECONOMICA';}
  else if (resRubro.state=='P') {T='PATENTE PUBLICIDAD';}
  else if (resRubro.state=='') {T='INMUEBLE';}

    
   if(this.personaEstado=='Natural')
        {
         
            if ((resNI!='')  || ( resRT !='') )
            {
              this.ContribuyenteService.getConsultaMixta(resNI,resRT, '', '', '', '', 'N', T)
                         .subscribe(
                           contribuyente => this.retornoLlamadaContribuyenteN(contribuyente),
                           error =>  this.retornoError('ERROR',error));
            }
            else
            {
          this.retornoError('AVISO','DEBE INGRESAR CARNET O REGISTRO TRIBUTARIO');            }
          
        }
   else
        {
          if ((resNIT!='') ||  ( resRT !='') )
            {         
             this.ContribuyenteService.getConsultaMixta(resNIT,resRT,'', '', '', '', 'J', T)
                         .subscribe(
                           contribuyente => this.retornoLlamadaContribuyenteJ(contribuyente),
                           error =>  this.errorMessage = <any>error);

            }
              else
            {
          this.retornoError('AVISO','DEBE INGRESAR NIT O REGISTRO TRIBUTARIO');
            }
          
          
        }
        
   document.getElementById("titulo2").innerHTML ="<strong>Mora Tributaria - Lista de Contribuyentes " +this.personaEstado +"</strong>";
     
      

  };

   


  onEdit(event: any): boolean {
    alert(event);
    //this.edited.next(event);
    return false;
  }

  onSelectRow(event: any) {
    alert(event);
    //this.grid.selectRow(event);
    //this.emitSelectRow(event);
  }

  eventSelectAE (event:any, estado:string):void {
    this.popup1.options = {
        header: "Seleccione una Opción",
        color: "#31708f", 
        widthProsentage: 38,  
        animationDuration: 1,
        showButtons: false, 
        //confirmBtnContent: "Puplicidad", // The text on your confirm button 
        //cancleBtnContent: "Actividad", // the text on your cancel button 
        confirmBtnClass: "btn btn-default", 
        cancleBtnClass: "btn btn-default", 
        animation: "bounceIn"
    };
    this.subPopup=true;
    this.popup1.show(this.popup1.options);
    if(estado=='N')
      {
      document.getElementById("cacbeceraActividades").innerHTML ="<strong>NUMERO DE IDENTIDAD:</strong> "+ event.data.identificacion +"&nbsp;&nbsp;<strong>NOMBRE:</strong> "+ event.data.primerNombre +"&nbsp;&nbsp;<strong>APELLIDO PATERNO:</strong> "+ event.data.primerApellido +"&nbsp;&nbsp;<strong>APELLIDO MATERNO:</strong> " + event.data.segundoApellido + "";
      this.var_PersonaID=event.data.idPersona;
      }
    else
      {
      document.getElementById("cacbeceraActividades").innerHTML ="<strong>NIT:</strong> "+ event.data.nit +"&nbsp;&nbsp;<strong>NOMBRE DE LA EMPRESA:</strong> "+ event.data.empresa +"";
      this.var_PersonaID=event.data.idEmpresa;
      }
  }


  public retornoLlamadaContribuyenteN (varC: any):void {
    this.datos7 = varC;//spBContribuyenteNumeroPersona; 
    console.log('datos7:',this.datos7);
  

    //document.getElementById("cacbeceraActividades").innerHTML ="<strong>Numero de Identidad:</strong> "+ this.datos1[0].identificacion +"&nbsp;&nbsp;<strong>Nombre:</strong> "+ this.datos1[0].NOMBRE +"&nbsp;&nbsp;<strong>Apellido Paterno:</strong> "+ this.datos1[0].APELLIDO +"&nbsp;&nbsp;<strong>Apellido Materno:</strong> " + this.datos1[0].segundoApellido + "";
    if (this.datos7.length !=0){
     // this.retornoError('AVISO IMPORTANTE¡¡','La liquidación fue generada con la Tasa de interes del 11.69% y cuente con la Multa por Omision Pago del 100% del Tributo Omitido, sin embargo la Administración Tributaria Municipal comunica que se encuentra vigente la Ley Municipal Autonómica Nº 212 de Regularización Voluntaria de Deudas Tributarias Municipales, mediante la cual los contribuyentes que paguen la deuda tributaria durante la vigencia de esta ley se beneficiaran con un interés único del 4% aplicable a todo el perido de la mora, Asimismo,si el pago se realiza antes de la primera publicación de la Liquidación por Determinación Mixta no cancelará la Multa Omisión Pago.');
 this.retornoError('AVISO IMPORTANTE¡¡','La Administración Tributaria Municipal comunica que se encuentra vigente la Ley Municipal Autonómica Nº 212 T.O.V. de Regularización Voluntaria de Deudas Tributarias Municipales, mediante la cual los contribuyentes que paguen la deuda tributaria durante la vigencia de esta ley se beneficiaran con una tasa de interés del 4% aplicable a todo el perido de la mora.');


      this.source7.load(varC);this.subBusqueda= false; this.subCaso=false;}
    else{this.retornoError('AVISO','No existen información de acuerdo a los parametros ingresados.'); this.source7.load(varC);}
  }

  public retornoLlamadaContribuyenteJ (varC: any):void {
    this.datos7 = varC;//spBContribuyenteNumeroPersona;
    if (this.datos7.length !=0){
 //     this.retornoError('AVISO IMPORTANTE¡¡','La liquidación fue generada con la Tasa de interes del 11.69% y cuente con la Multa por Omision Pago del 100% del Tributo Omitido, sin embargo la Administración Tributaria Municipal comunica que se encuentra vigente la Ley Municipal Autonómica Nº 212 de Regularización Voluntaria de Deudas Tributarias Municipales, mediante la cual los contribuyentes que paguen la deuda tributaria durante la vigencia de esta ley se beneficiaran con un interés único del 4% aplicable a todo el perido de la mora, Asimismo,si el pago se realiza antes de la primera publicación de la Liquidación por Determinación Mixta no cancelará la Multa Omisión Pago.');
     this.retornoError('AVISO IMPORTANTE¡¡','La Administración Tributaria Municipal comunica que se encuentra vigente la Ley Municipal Autonómica Nº 212 T.O.V. de Regularización Voluntaria de Deudas Tributarias Municipales, mediante la cual los contribuyentes que paguen la deuda tributaria durante la vigencia de esta ley se beneficiaran con una tasa de interés del 4% aplicable a todo el perido de la mora.');
 
      this.source7.load(varC);

      //console.log('T',this.datos1);
      
      //document.getElementById("cacbeceraActividades").innerHTML ="<strong>NIT:</strong> "+ this.datos1[0].NIT +"&nbsp;&nbsp;<strong>Nombre de la Empresa:</strong> "+ this.datos1[0].NOMBRE +"";
      //this.var_identificacion= this.datos1[0].nit;
      //this.var_nombre= this.datos1[0].empresa;
      //this.var_paterno= '';
      //this.var_materno= '';
      this.subBusqueda= false; this.subCaso=false;
    }
    else{this.retornoError('AVISO','No existen información de acuerdo a los parametros ingresados.'); this.source7.load(varC);}
  }




  public retornoError (errortitulo: string,error: string):void{
    this.var_idA='';
    this.popup2.options = {
      header: errortitulo,
      color: "#e18f35", 
      widthProsentage: 38,  
      animationDuration: 1,
      showButtons:false, 
      //confirmBtnContent: "Puplicidad", // The text on your confirm button 
      cancleBtnContent: "Salir", // the text on your cancel button 
      //confirmBtnClass: "btn btn-default", 
      cancleBtnClass: "btn btn-default", 
      animation: "bounceInDown"
    };
    this.Message=error;
    this.subPopup2=true;
    this.popup2.show(this.popup2.options);
  }


  
  
  public getData():void {
    //alert("res");
  }

  pintarPersona():void {
    //this.pintarTabla();
    let sHTMLcarpeta='';
    if(this.botonEstado=='Aqui para Buscar Natural')
    {
      this.personaEstado='Natural';
      this.botonEstado='Aqui para Buscar Juridico';this.subPersona = true;
      this.limpiarBusqueda(this.personaEstado);
    }
    else
    {
      this.personaEstado='Juridico';
      this.botonEstado='Aqui para Buscar Natural';this.subPersona = false;
      this.limpiarBusqueda(this.personaEstado);
    }
    //document.getElementById("pintarDatos").innerHTML =sHTMLcarpeta;
  }

  verAtras():void {
      if(this.subTabla ==true && this.subCaso==false)
      {
        if(this.subTabla2 ==true)
        {
          this.subTabla2=false;
        }
        else
        {
          this.subTabla =false; document.getElementById("titulo2").innerHTML ="<strong>Deuda Mora - Lista de Contribuyente " +this.personaEstado +"</strong>";
        }
      }
      else
      {this.subBusqueda= true; this.subTabla =false;this.subTabla2=false;this.limpiarBusqueda(this.botonEstado);this.subCaso=false; let vacio: any [] = [];this.source.load(vacio);this.source1.load(vacio);}
  }

  verBusqueda():void {
    this.subBusqueda= true; this.subTabla =false;this.subTabla2=false; this.limpiarBusqueda(this.botonEstado);this.subCaso=false;
    let vacio: any [] = [];this.source.load(vacio);this.source1.load(vacio);
  }

  closeMessage():void {
    this.subPopup2=false;
  }

  limpiarBusqueda(estado: string):void {
    if(estado!='Natural')
    {
      this.personal.ci='';this.personal.nombre='';this.personal.paterno='';this.personal.materno='';this.personal.caso='';this.personal.padron=''; this.personal.reg='';
    }
    else
    {
      this.personal.nit='';this.personal.empresa='';this.personal.caso='';this.personal.padron='';this.personal.reg='';
    }
    
  }
  
}


export class Personal {
  ci: string;
  reg: string;
  nombre: string;
  paterno: string;
  materno: string;
  nit: string;
  empresa: string;
  padron: string;
  caso: string;
}