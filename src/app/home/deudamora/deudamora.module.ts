import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { Ng2SmartTableModule } from 'ng2-smart-table';
//import { AppRoutingModule } from './app-routing.module';
//import { HomeModule } from './home/home.module';
import { DeudaMoraComponent } from '../deudamora/deudamora.component';

@NgModule({
  declarations: [
    DeudaMoraComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,

    Ng2SmartTableModule
  ],
  providers: [],
  bootstrap: [DeudaMoraComponent]
})
export class AppModule { }
